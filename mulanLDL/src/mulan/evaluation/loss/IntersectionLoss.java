/*
 *    This program is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package mulan.evaluation.loss;

/**
 * Implementation of the Chebyshev loss function.
 * 
 * @author Christina Papagiannopoulou
 * @version 2013.6.13
 */
public class IntersectionLoss extends ConfidenceLossFunctionBase {

    double epsilon = 0.00000001;

    public void setEpsilon(double epss) {
        epsilon = epss;
    }

    @Override
    public String getName() {
        return "Intersection Loss";
    }

    @Override
    public double computeLoss(double[] confidences, double[] groundTruth) {

        double sum = 0.0;
        for (int labelIndex = 0; labelIndex < groundTruth.length; labelIndex++) {
            	sum += Math.min(confidences[labelIndex],groundTruth[labelIndex]);
        }
        
        return sum;

    }
}
