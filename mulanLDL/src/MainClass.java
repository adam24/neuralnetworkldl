import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import mulan.classifier.lazy.MLkNN;
import mulan.classifier.meta.HOMER;
import mulan.classifier.meta.RAkEL;
import mulan.classifier.neural.BPMLL;
import mulan.classifier.neural.BPMLLDivider;
import mulan.classifier.neural.BPMLLE2;
import mulan.classifier.neural.BPMLLE345;
import mulan.classifier.neural.BPMLLIdea1;
import mulan.classifier.neural.BPMLLIdea2;
import mulan.classifier.neural.BPMLLLDL;
import mulan.classifier.neural.model.Neuron;
import mulan.classifier.transformation.BinaryRelevance;
import mulan.classifier.transformation.CalibratedLabelRanking;
import mulan.classifier.transformation.ClassifierChain;
import mulan.classifier.transformation.EnsembleOfClassifierChains;
import mulan.data.InvalidDataFormatException;
import mulan.data.MultiLabelInstances;
import mulan.evaluation.Evaluator;
import mulan.evaluation.MultipleEvaluation;
import mulan.evaluation.measure.CanberraLoss;
import mulan.evaluation.measure.CosineLoss;
import mulan.evaluation.measure.IntersectionLoss;
import mulan.evaluation.measure.KullbackLeiblerLoss;
import mulan.examples.CrossValidationExperiment;
import weka.classifiers.trees.J48;

public class MainClass {

    public static void main(String[] args) throws Exception {

	String inputDir = args[1];
	String outputPath = args[2];
        PrintWriter outFile = new PrintWriter(new FileOutputStream(new File(outputPath), true));
        outFile.append(
                "benchmark;i;method;time;Chebyshev Loss;Clark Loss;Canberra Loss;Kullback Leibler Loss;"
                        + "Cosine Loss;Intersection Loss;\n");
        String[] benchmarks = new String[]{"Yeast_alpha",
                "Yeast_cdc",
                "Yeast_cold",
                "Yeast_diau",
                "Yeast_dtt",
                "Yeast_elu",
                "Yeast_heat",
                "Yeast_spo",
                "Yeast_spo5",
                "Yeast_spoem",
                "Human_Gene",
                "Movie",
                "Natural_Scene",
                "SBU_3DFE",
                "SJAFFE"};

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < benchmarks.length; j++) {
                String benchmark = benchmarks[j];

                String arffFilename = inputDir + "\\" + benchmark + "\\" + benchmark + ".arff";
                String xmlFilename = inputDir + "\\" + benchmark + "\\" + benchmark + ".xml";

                MultiLabelInstances dataset = new MultiLabelInstances(arffFilename, xmlFilename);

                BPMLLLDL learner2 = new BPMLLLDL();
                if (benchmark == "yeast")
                    learner2.setNormalizeAttributes(false);
                else
                    learner2.setNormalizeAttributes(true);

                Evaluator eval = new Evaluator();
                eval.isInnerCV = false;
                eval.outFile = outFile;
                eval.benchmark = benchmark;
                //eval.errorName = errorNames[k];
                eval.divider = new double[]{1, 1, 1};
                MultipleEvaluation results;

                int numFolds = 10;

                long start = System.currentTimeMillis();
                results = eval.crossValidate(learner2, dataset, numFolds);
                long time = System.currentTimeMillis() - start;
                outFile.append(benchmark + ";" + i + ";" + "BPLDL" + ";" + time/1000 + ";");
                WriteResultsToFile(results, outFile);
                System.out.println(benchmark + ";" + i + " Chebyshev Loss: " + results.getMean("Chebyshev Loss"));

                outFile.flush();
            }
        }
        outFile.close();
        System.out.println("---KONIEC---");
    }

    static void WriteResultsToFile(MultipleEvaluation results, PrintWriter outFile) {
        outFile.append(String.valueOf(results.getMean("Chebyshev Loss")) + ";");
        outFile.append(String.valueOf(results.getMean("Clark Loss")) + ";");
        outFile.append(String.valueOf(results.getMean("Canberra Loss")) + ";");
        outFile.append(String.valueOf(results.getMean("Kullback Leibler Loss")) + ";");
        outFile.append(String.valueOf(results.getMean("Cosine Loss")) + ";");
        outFile.append(String.valueOf(results.getMean("Intersection Loss")) + ";");
        outFile.append("\n");
    }
}
