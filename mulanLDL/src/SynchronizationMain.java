import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SynchronizationMain implements Runnable{

    private static int EVENTS = 100000;

    private List<Event> cachedEvents = Collections.synchronizedList(new ArrayList<>());
    private boolean running = true;

    public static void main(String[] args) {
        long time  = System.currentTimeMillis();
        System.out.println("Problem: What is wrong with synchronization in this example and why?");

        System.out.println("Producing events thread: " + Thread.currentThread());
        ExecutorService executor = Executors.newSingleThreadExecutor();
        SynchronizationMain synchronizationMain = new SynchronizationMain();
        executor.submit(synchronizationMain);
        for(int i=0; i<EVENTS; ++i){
            synchronizationMain.addEvent(new Event());
        }
        synchronizationMain.stop();
        executor.shutdown();
        System.out.println("Time: "+(System.currentTimeMillis() - time));
    }

    public void stop(){
        running = false;
    }

    public void run() {
        System.out.println("Processing events thread: " + Thread.currentThread());
        while(running){
            processCachedEvents();
        }
        processCachedEvents();
        System.out.println("Expected " + EVENTS + " events, got " + allEvents.size());
    }

    private List<Event> allEvents = new ArrayList<>();

    public void addEvent(Event event){
        cachedEvents.add(event);
    }

    public void processCachedEvents(){
        allEvents.addAll(cachedEvents);
        cachedEvents.clear();
    }

    private static class Event{
        private final long time;

        Event(){
            time = System.currentTimeMillis();
        }
    }
}
